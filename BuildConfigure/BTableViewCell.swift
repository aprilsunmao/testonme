//
//  BTableViewCell.swift
//  BuildConfigure
//
//  Created by April Yang on 2020/3/24.
//  Copyright © 2020 April Yang. All rights reserved.
//

import UIKit

protocol BTableViewCellProtocol: class {
    func pushTest()
}

class BTableViewCell: UITableViewCell {
    weak var delegate: BTableViewCellProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func push(_ sender: Any) {
        self.delegate?.pushTest()
    }
}
