//
//  ViewController.swift
//  BuildConfigure
//
//  Created by April Yang on 2020/3/24.
//  Copyright © 2020 April Yang. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        print("hello world")
    }

    @IBAction func push(_ sender: Any) {
        
        #if VERSIONONE
        let b = BViewController()
        self.navigationController?.pushViewController(b, animated: true)
        #elseif VERSIONTWO
        self.navigationController?.pushViewController(CViewController(), animated: true)
        #endif
    }
    
}

