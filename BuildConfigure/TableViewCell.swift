//
//  TableViewCell.swift
//  BuildConfigure
//
//  Created by April Yang on 2020/3/24.
//  Copyright © 2020 April Yang. All rights reserved.
//

import UIKit

protocol TableViewCellProtocol: class {
    func test()
}

class TableViewCell: UITableViewCell {

    var delegate: TableViewCellProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func tap(_ sender: Any) {
        self.delegate?.test()
    }
}
