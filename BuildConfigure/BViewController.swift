//
//  BViewController.swift
//  BuildConfigure
//
//  Created by April Yang on 2020/3/24.
//  Copyright © 2020 April Yang. All rights reserved.
//

import UIKit

class BViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "TableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "cell")
        tableView.register(UINib(nibName: "BTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "bcell")
    }

    func atest()
    {
        print("test")
    }

    func lalala()
    {

    }
}

extension BViewController: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.item % 2 == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
            cell.delegate = self
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "bcell", for: indexPath) as! BTableViewCell
            cell.delegate = self
            return cell
        }
        
    }
    
    
}

extension BViewController: TableViewCellProtocol, BTableViewCellProtocol
{
    func test() {
        print("TableViewCellProtocol->tap")
    }
    
    func pushTest() {
        print("push")
    }
}
