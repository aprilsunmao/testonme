//
//  AppDelegate.swift
//  BuildConfigure
//
//  Created by April Yang on 2020/3/24.
//  Copyright © 2020 April Yang. All rights reserved.
//

import UIKit

#if DEV
    let SOME_SERVICE_KEY = "SomeKeyForDEV"
#elseif PRO
    let SOME_SERVICE_KEY = "SomeKeyForPRO"
#endif

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
       print("SOME_SERVICE_KEY-:\(SOME_SERVICE_KEY)")
        
        #if PRODEBUG
        print("ENV-PRODEBUG")
        #elseif PRORELEASE
        print("ENV-PRORELEASE")
        #elseif DEVDEBUG
        print("ENV-DEVDEBUG")
        #elseif DEVRELEASE
        print("ENV-DEVRELEASE")
        #endif
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

